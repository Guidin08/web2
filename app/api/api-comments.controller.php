<?php

require_once 'app/models/comments.model.php';
require_once 'app/api/api.view.php';

class ApiCommentsController{

    private $model;
    private $view;
    private $data;
   

    function __construct(){
        $this->model = new CommentsModel();
        $this->view = new APIView();
        /**Obtengo lo que tengo por post, como texto */
        $this->data = file_get_contents('php://input');
    }

    /**Funcion que convierte la variable data en JSON */
   function getData($params = null){
        return json_decode($this->data);
    }

    public function add($params = null){
        $body = $this->getData();
        $valoracion = $body->valoracion;
        $comentario = $body->comentario;
        $user = $body->id_user;
        $employee = $body->id_employee;     
        
        $id = $this->model->insert($comentario,$valoracion,$user,$employee);
        if ($id){
            $this->view->response($id, 200);
        } else {
            $this->view->response("No se pudo insertar", 404);
        }   
        
    }

    public function getAll($params = null){
        $idcomment = $params[':ID'];
        $allComments = $this->model->getAll($idcomment);
        $this->view->response($allComments,200);
    }

    public function delete($params = null){
        $idcomment = $params[':ID'];
        $success = $this->model->remove($idcomment);
        if ($success){
            $this->view->response("El comentario se elimino satisfactoriamente", 200);
        } else {
            $this->view->response("El comentario no puede ser eliminado", 404);
        }
    }
}

