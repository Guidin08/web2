<?php
require_once 'app/models/employees.model.php';
require_once 'app/views/profession.view.php';


require_once 'app/api/api.view.php';
require_once 'app/models/profession.model.php';


class ApiEmployeesController{

    private $model;
    private $view;

    function __construct(){
        $this->model = new EmployeesModel();
        $this->view = new APIView();

        /**Obtengo lo que tengo por post, como texto */
        $this->data = file_get_contents('php://input');
    }
    private function getData($params = null){
        return json_decode($this->data);
    }
    /* Obtiene todos los trabajadores */
    public function getAll($params =null){
        $allworkers = $this->model->getAll();
        $this->getData($allworkers);
        if ($allworkers){
            $this->view->response($allworkers, 200);
        } else {
            $this->view->response("No se encontraron empleados", 500);
        }
        
    }
    /* Obtiene un solo trabajador */
    public function get($params =null){
        $oneworker = $this->model->get($params[':ID']);
        $this->getData($oneworker);
        if ($oneworker){
            $this->view->response($oneworker, 200);
        } else {
            $this->view->response("No se encontraron empleados", 500);
        }
        
    }
    /* con esta funcion se obtiene una cantidad limitada de trabajadores para manejar la paginacion */
    public function getPagination($params =null){
        $pagWorker = $this->model->getPagination($params[':ID']);
        $this->getData($pagWorker);        
        $this->view->viewEmpl($pagWorker,200);
    }

    function getadv($params){   
        $listemploy = $this->model->getByCategoryAdv($params[':FILTER'],$params[':POS']);
        $this->getData($listemploy);
        if ($listemploy){
             $this->view->viewEmpl($listemploy,200);
         }else{
             $msg='No existen empleados para esta categoria';
             $this->view->showError($msg);
         }
    }


    
}