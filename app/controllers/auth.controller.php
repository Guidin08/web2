<?php
include_once 'app/models/auth.model.php';
include_once 'app/views/profession.view.php';


class AuthController{
    private $modelUser;
    private $view;
    private $authHelper;

    function __construct() {
        $this->modelUser = new AuthModel();
        $this->view = new JobView();
        $this->authHelper = new AuthHelper();
    }
  
    /** muestra el inicio de sesion */
    function initSesion(){
      $this->view->popUpInitSesion();        
    }

    /** cierra sesion */
    function logout(){
      $this->authHelper->logOut();
      header("Location: " . BASE_URL . "home"); 

    }

    /** obtiene los datos de usuario y otorga permisos segun corresponda (admin o usuario) */
    function verinicio(){
      $user = $_POST['usuario'];
      $pass = $_POST['password'];
      $this->goHome($user, $pass);
    }

    /** Se dirije al home luego de iniciar sesion */
    function goHome($user, $pass){
      $find = $this->modelUser->getByUser($user);
      if ($find && password_verify ($pass , $find->pass)){
        session_start();
        $_SESSION['USUARIO']=$find->user;
        $_SESSION['ADMIN']=$find->type_user;
        $_SESSION['ID_USUARIO']=$find->id;    
        header("Location: " . BASE_URL . "home"); 
      }else{
          $this->view->popUpInitSesion('Usuario incorrecto');
      } 
    }

    /** Crea un usuario no administrador */
    function createUser(){
      $user = $_POST['usuario'];
      $pass = $_POST['password'];
      if (!(empty($user) && empty($pass)) && (strlen($user) >= 5 && strlen($pass) >= 5)){
        $sucess = $this->modelUser->getByUser($user);
        if (!($sucess)){
          $pass_hash = password_hash ($pass , PASSWORD_DEFAULT);
          $this->modelUser->insert($user, $pass_hash);
          $this->goHome($user, $pass);
        } else {
          $this->view->createAcountUser('Ingrese un usuario distinto');
        }
      } else {
        $this->view->createAcountUser('Error al registrar, recuerde que tiene que ingresar mas de 5 caracteres en ambos casos');
      }
    }

    /** Cambia a un usuario de no administrador a administrador */
    function changeUser($id){
      if ($this->authHelper->checkLoginAdmin()){
        $success = $this->modelUser->update($id);
        if ($success) {
          $users = $this->modelUser->getAll();
          $this->view->showMenuManageUsers($users);
        }
      }else{
          $this->view->popUpInitSesion('Debe estar logueado');
      }
    }

    /** Elimina un usuario */
    function deleteUser($id){
      if ($this->authHelper->checkLoginAdmin()){
        $success = $this->modelUser->delete($id);
        if ($success) {
          $users = $this->modelUser->getAll();
          $this->view->showMenuManageUsers($users);
        }
      }else{
          $this->view->popUpInitSesion('Debe estar logueado');
      }
    }
}