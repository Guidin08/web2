<?php
include_once 'app/models/profession.model.php';
include_once 'app/models/employees.model.php';
include_once 'app/models/auth.model.php';
include_once 'app/views/profession.view.php';
include_once 'app/helpers/auth.helper.php';


class JobController{
    private $modelEmployees;
    private $modelProfessions;
    private $modelUsers;
    private $view;
    private $authHelper;

    function __construct() {
        $this->modelEmployees = new EmployeesModel();
        $this->modelProfessions = new ProfessionModel();
        $this->modelUsers = new AuthModel();
        $this->view = new JobView();
        // Incluye el helper del control de usuario
        $this->authHelper = new AuthHelper();
    }

    function showHome(){
        //llama a la clase del taskview para mostrar el home
        $professions=$this->modelProfessions->getAll();
        $this->view->showProfessions($professions);
        $show=$this->modelEmployees->getAll();   
        $this->view->showEmployees($show);
    }

     /** Pide datos para mostrar menu del administrador, para que este pueda hacer ABM de personas y categorias */
    function showMenuManage(){
        if ($this->authHelper->checkLogin()){
            $employees=$this->modelEmployees->getAll();
            $professions=$this->modelProfessions->getAll();
            $this->view->showMenuManageCat($employees, $professions);
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }
        
    }
  
    /** Lista todas las profesiones */ 
    function showCategories(){
        $professions=$this->modelProfessions->getAll();        
        $this->view->showProfessions($professions);
    }

    /** Pide datos para mostrar menu del administrador, para que este pueda hacer ABM de personas y categorias */
    function showMenuManageCat($msg){
        if ($this->authHelper->checkLoginAdmin()){
            $categories=$this->modelProfessions->getAll();
            $this->view->showMenuManageCat($categories, $msg);
        }else{
            $this->view->popUpInitSesion('Debe estar logueado y ser administrador');
        }
        
    }

    /** va al menu para administrar empleados */
    function showMenuManageWork($msg){
        if ($this->authHelper->checkLoginAdmin()){
            $employees=$this->modelEmployees->getAll();
            $this->view->showMenuManageWork($employees, $msg);
        }else{
            $this->view->popUpInitSesion('Debe estar logueado y ser administrador');
        }
    }

    /** va al menu para administrar empleados */
    function showMenuManageUsers($msg){
        if ($this->authHelper->checkLoginAdmin()){
            $users = $this->modelUsers->getAll();
            $this->view->showMenuManageUsers($users, $msg);
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }
    }

    /** Agrega una profesion */
    function addCategory(){
        if ($this->authHelper->checkLoginAdmin()){
            $profession = $_POST['inputCategory'];
            if(empty($profession) || $profession == $this->modelProfessions->getByName($profession)->profesion){
                $this->showMenuManageCat("El nombre de la profesion esta vacia o ya existe");
                die();
            }
            $id = $this->modelProfessions->insert($profession);
            header("Location: " . BASE_URL . "manageCategory");
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }
    }

    /** Elimina una profesion */
    function deleteCategory($id){        
        if ($this->authHelper->checkLoginAdmin()){
            $success = $this->modelProfessions->remove($id);
            if ($success) {
                $this->view->showInfoCat("Categoria eliminada", "manageCategory");
            } else {
                $this->view->showInfoCat("Error al eliminar la categoria", "manageCategory");
            }
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }
        
    }

    /** Elimina un empleado */
    function deleteWorker($id){
        if ($this->authHelper->checkLoginAdmin()){
            $this->modelEmployees->remove($id);
            $this->view->showInfoCat("Trabajador eliminado", "manageWorkers");
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }
    }

    /** muestra el cuadro de edicion de la profesion seleccionada */
    function showEditCategory($id){
        if ($this->authHelper->checkLoginAdmin()){
            $category = $this->modelProfessions->get($id);
            $this->view->showEditCat($category);
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }
    }

    /** actualiza la profesion */
    function updateCategory(){
        if ($this->authHelper->checkLoginAdmin()){
            $name = $_POST['nameCategory'];
            $id = $_POST['idCategory'];
            if(empty($name) || $name == $this->modelProfessions->getByName($name)->profesion){
                $this->showMenuManageCat("El nombre de la profesion esta vacia o ya existe");
                die();
            }
            $category = $this->modelProfessions->update($id, $name);
            header("Location: " . BASE_URL . "manageCategory"); 
            
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }       
    }

    /** muestra el cuadro de edicion con la persona seleccionada */
    function showEditPerson($id){
        if ($this->authHelper->checkLoginAdmin()){
            $person = $this->modelEmployees->get($id);
            $professions = $this->modelProfessions->getAll();
            $this->view->showPers($person, $professions);
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }
              
    }

    /** actualiza los datos luego de ser modificados ADMINISTRADOR */
    function updatePerson(){
        if ($this->authHelper->checkLoginAdmin()){
            $id = $_POST['id'];
            $dni = $_POST['dni'];
            $name = $_POST['nombre'];
            $lastname = $_POST['apellido'];
            $mail = $_POST['email'];
            $tel = $_POST['telefono'];
            $prof = $_POST['profesion'];
            $photo = $this->photoview();
            if ($photo == null){
                $photo = $_POST['foto'];
            }
            $campos = array($dni, $name, $lastname, $mail, $tel, $prof);
            if (!($this->comprobarCampos($campos))){
                $this->showMenuManageWork('Debe completar todos los campos');
                die();
            }
            $this->modelEmployees->update($id, $dni, $name, $lastname, $mail, $tel, $prof, $photo);
            header("Location: " . BASE_URL . "manageWorkers");
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }       
    }

    /** lista empleados para la edicion de los mismos */
    function showAgreePerson(){
        if ($this->authHelper->checkLoginAdmin()){
            $person = (object) [
                "id_emp" => "",
                "nombre" => "",
                "apellido" => "",
                "id_prof" => "",
                "email" => "",
                "telefono" => "",
                "dni" => "",
                "emp_image" => ""
            ];
            $categories = $this->modelProfessions->getAll();
            $this->view->showPers($person, $categories);
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }        
    }

    /** agrega una persona ADMINISTRADOR */
    function addPerson(){
        if ($this->authHelper->checkLoginAdmin()){
            $dni = $_POST['dni'];
            $name = $_POST['nombre'];
            $lastname = $_POST['apellido'];
            $mail = $_POST['email'];
            $tel = $_POST['telefono'];
            $prof = $_POST['profesion'];
            $photo = $this->photoview();
            $campos = array($dni, $name, $lastname, $mail, $tel, $prof);
            if (!($this->comprobarCampos($campos))){
                $this->showMenuManageWork('Debe completar todos los campos');
                die();
            }
            $employee = $this->modelEmployees->getByDni($dni);
            if ($employee && ($dni == $this->modelEmployees->getByDni($dni)->dni)){
                $this->showMenuManageWork('Ya existe una persona con ese DNI');
                die();
            }
            $sucess = $this->modelEmployees->insert($dni, $name, $lastname, $mail, $tel, $prof, $photo);
            header("Location: " . BASE_URL . "manageWorkers");
        }else{
            $this->view->popUpInitSesion('Debe estar logueado');
        }
    }

    /** comprueba que los campos no esten vacios durante la edicion */
    function comprobarCampos($campos){
        foreach($campos as $campo){
            if (empty($campo)){
                return false;
                die();
            }
        }
        return true;
    }

    /** obtiene los datos de las personas */
    function verPerfil($persona){
        $ok=$this->authHelper->checkLogin();
        $perfil = $this->modelEmployees->get($persona); 
        $this->view->perfil($perfil,$ok);
    }

    /** redirije a la pagina para crear una cuenta */
    function createAcount(){
        $jobs=$this->modelProfessions->getAll();
        $this->view->crearAcount($jobs);
    }
    
    function createAcountUser(){
        $this->view->createAcountUser();
    }    


    function showOptions(){
        $this->view->optionsAcount();
    }

    /** crea una cuenta  */
    function upPerson(){

        if (!$this->authHelper->checkLogin()){
            $lastname = $_POST['apellido'];
            $name = $_POST['nombre'];
            $dni = $_POST['dni'];
            $email = $_POST['email'];
            $phone = $_POST['telefono'];
            $categ = $_POST['oficio'];
           
            $this->showCategories();
            $photo=$this->photoview();
            
            if ($this->modelEmployees->getByDni($dni)!=null){
                $this->view->showError('No se pudo crear el usuario, ya existe uno con ese DNI');
                
            }else{
                $id=$this->modelProfessions->getByName($categ);
                $this->modelEmployees->insert($dni,$name,$lastname,$email,$phone,$id->id,$photo);
                $this->view->showGood($name,$photo);
            }
        }
       
    }

    function uniqueSaveName($realName, $tempName) {        
        $filePath = "./img/usersimg" . uniqid("", true) . "." . strtolower(pathinfo($realName, PATHINFO_EXTENSION));
        move_uploaded_file($tempName, $filePath);
        return $filePath;
    }

    function photoview(){
        // inserto la imagen en la DB
        if($_FILES['input_name']['type'] == "image/jpg" || 
            $_FILES['input_name']['type'] == "image/jpeg" || 
            $_FILES['input_name']['type'] == "image/png" ) 
        {
           $namePhoto = $this->uniqueSaveName($_FILES['input_name']['name'], $_FILES['input_name']['tmp_name']);
           return $namePhoto;
        }
        else {
           return null;
        }

    }
   

}