<?php
class AuthHelper{

    function __construct(){
        $this->logIn();
    }
    
    /** Funcion que se verifica si esta conectado, sino lo conecta */
    function logIn(){
        if (session_status() != PHP_SESSION_ACTIVE) {
            session_start();
        }
    }
    
    /** controla que el usuario este logueado */
    function checkLogin(){
        if (isset($_SESSION['USUARIO'])) {
            return true;
            die() ;
        }
        return false;
    }

    /** controla que el usuario este logueado y sea admin */
    function checkLoginAdmin(){
        if (isset($_SESSION['USUARIO']) && $_SESSION['ADMIN'] == 1) {
            return true;
            die() ;
        }
        return false;
    }

    /** cierra la session */
    function logOut(){
        session_destroy();
        header("Location: " . BASE_URL . "iniciarsesion");
    }


}