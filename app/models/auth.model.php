<?php


class AuthModel{
    private $database;

    function __construct() {
        $this->database = $this->connect();
    }

    /** abro la coneccion */
    private function connect() {
        $database = new PDO('mysql:host=localhost;'.'dbname=db_jobs;charset=utf8', 'root', '');  
        return $database;
    }
    /** busca que exista el usuario */
    function getByUser($user){
        $query = $this->database->prepare ('SELECT * FROM users WHERE user=?');
        $query->execute([$user]);
        $find=$query->fetch(PDO::FETCH_OBJ);
        return $find;
    }
    
    /** Obtiene todos los usuarios */
    function getAll(){
        $query = $this->database->prepare('SELECT * from users');
        $query->execute();
        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    function insert($user, $pass){
        $query = $this->database->prepare('INSERT INTO users (user, pass, type_user) VALUES (?,?,?)');
        $query->execute([$user, $pass, 0]);
        return $this->database->lastInsertId();
    }

    function update($id){
        $query = $this->database->prepare('UPDATE users SET type_user = ? WHERE id = ?');
        return $query->execute([1, $id]);
    }

    function delete($id){
        $query = $this->database->prepare('DELETE FROM users WHERE id = ?');
        return $query->execute([$id]);
    }


}