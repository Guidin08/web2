<?php


class CommentsModel{
    private $database;

    function __construct() {
        $this->database = $this->connect();
    }
    
    
    /** Abro la conexión de la base de datos */
    private function connect() {
        $database = new PDO('mysql:host=localhost;'.'dbname=db_jobs;charset=utf8', 'root', '');  
        return $database;
    }

    /** Devuelve todas los comentarios */
    function getAll($id){
       
        $query = $this->database->prepare('SELECT c.*, u.user, u.id as id_usuario FROM comments c INNER JOIN users u on c.id_user = u.id WHERE id_employee=? ORDER BY c.id DESC');
        $query->execute([$id]);
        $comments = $query->fetchAll(PDO::FETCH_OBJ);
        return $comments;
    }
    /** Inserta un comentario a la BBDD */  
    function insert($comentario,$valoracion,$user,$employee){
        $query = $this->database->prepare('INSERT INTO `comments`(`comentario`, `valoracion`,`id_user`,`id_employee`) VALUES (?,?,?,?)');
        $query->execute([$comentario, $valoracion,$user,$employee]);
        return $this->database->lastInsertId();
    }

    function remove($id){
        $query = $this->database->prepare('DELETE FROM comments where id = ?');
        return $query->execute([$id]);
    }



}