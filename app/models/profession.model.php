<?php


class ProfessionModel{
    private $db;

    function __construct() {
        //conecto a la base de datos
        $this->database = $this->connect();
    }

    //abro la coneccion
    private function connect() {
        $database = new PDO('mysql:host=localhost;'.'dbname=db_jobs;charset=utf8', 'root', '');  
        //$database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        return $database;
    }
    
    /** Elimina una profesion */
    function remove($id){
        $query = $this->database->prepare('DELETE FROM professions where id = ?');
        return $query->execute([$id]);
    }

    /** Devuelve todas las profesiones */
    function getAll(){
        $query = $this->database->prepare('SELECT * FROM professions ORDER BY id DESC');
        $query->execute();
        $professions = $query->fetchAll(PDO::FETCH_OBJ);
        return $professions;
    }
   
    /** Agrega una profesion */
    function insert($name){
        $query = $this->database->prepare('INSERT INTO professions (profesion) VALUES (?)');
        return $query->execute([$name]);
    }
    
    /** Devuelve una profesion dependiendo el parametro recibido */
    function get($id){
        $query = $this->database->prepare('SELECT * FROM professions WHERE id=?');
        $query->execute([$id]);
        $profession = $query->fetch(PDO::FETCH_OBJ);
        return $profession;
    }
 
    /** Actualiza una profesion */
    function update($id, $name){
        $query = $this->database->prepare('UPDATE professions SET profesion = ? WHERE id = ?');
        return $query->execute([$name, $id]);
    }

    /** Devuelve una profesion segun un nombre pasado por parametro */
    function getByName($name){
        $query = $this->database->prepare('SELECT * FROM professions WHERE profesion=?');
        $query->execute([$name]);
        $profession = $query->fetch(PDO::FETCH_OBJ);
        return $profession;
    }

}

