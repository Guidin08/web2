-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-11-2020 a las 22:54:13
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_jobs`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comentario` varchar(250) NOT NULL,
  `valoracion` int(10) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`id`, `comentario`, `valoracion`, `id_user`, `id_employee`) VALUES
(1, 'opinion2', 1, 1, 4),
(2, 'opinion2', 1, 1, 4),
(4, 'Sin Comentario de parte del usuario', 5, 1, 5),
(5, 'Sin Comentario de parte del usuario', 5, 1, 5),
(6, 'Sin Comentario de parte del usuario', 5, 1, 30),
(7, 'Sin Comentario de parte del usuario', 5, 1, 30),
(36, 'perfectirigillo', 5, 1, 34),
(37, 'Muy religiosillo', 4, 13, 34),
(50, '¡Genializoso y estupendástico!', 4, 1, 34),
(58, 'Solo le importa su dinero', 5, 1, 37),
(60, 'Ebrio como pocos', 5, 15, 36),
(62, 'comentario 3', 3, 1, 36);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `id_profession` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `telefono` int(20) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `emp_image` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `employees`
--

INSERT INTO `employees` (`id`, `id_profession`, `dni`, `apellido`, `email`, `telefono`, `nombre`, `emp_image`) VALUES
(4, 3, 2222, 'Szyslak', 'moes@fox.com.ar', 111365, 'Moe', './img/usersimg5fb576041ce6b4.61433995.png'),
(5, 7, 3333, 'Simpson', 'lisa@fox.com.ar', 6549879, 'Lisa', './img/usersimg5fb575b294aca4.39746496.png'),
(30, 6, 1234, 'Simpson', 'homerosimpson@fox.com.ar', 987654321, 'Homero', './img/usersimg5fb5758f546381.80591848.jpg'),
(32, 8, 23423, 'Simpson', 'bartolomeo@fox.com.ar', 2147483647, 'Bart', './img/usersimg5fb575611bab27.38210450.png'),
(34, 4, 654321, 'Flanders', 'ned@flanders.com.ar', 987, 'Ned', './img/usersimg5fb5b133582c29.02431248.jpg'),
(35, 6, 1111, 'Simpson', 'margie@s.com.ar', 555598, 'Marge', './img/usersimg5fbeb566dc7237.80573481.jpg'),
(36, 6, 4444, 'Gumble', 'barney@duff.com.ar', 963852741, 'Barney', './img/usersimg5fbeb5988a25d0.25989136.jpeg'),
(37, 6, 5555, 'Burns', 'burns@dolar.com', 7418529, 'Montgomery', './img/usersimg5fbeb5d41916b5.43846476.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `professions`
--

CREATE TABLE `professions` (
  `id` int(11) NOT NULL,
  `profesion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `professions`
--

INSERT INTO `professions` (`id`, `profesion`) VALUES
(1, 'Peluqueria'),
(2, 'Carpinteria'),
(3, 'Albañileria'),
(4, 'Cerrajero'),
(5, 'Gasista'),
(6, 'Plomero'),
(7, 'Soldador'),
(8, 'Pintor'),
(9, 'Chofer');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `pass` varchar(80) NOT NULL,
  `type_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `user`, `pass`, `type_user`) VALUES
(1, 'admin', '$2y$10$7qgxFJ3W.TyEkBe7WZumoeuEL0AQX/OpieKPisLyt9jJ2fyElD65a', 1),
(13, 'federico', '$2y$10$TJPNwnlMMm1cIcSlgmXYXOK9tH4UOBVca/HKqq.DLUxctjxMnpGfG', 0),
(15, 'guido', '$2y$10$07DQhJGLhhe7zFXXZPzpNeaBUJArJK4.ceHC2XqeVYsiqR.4mB7XG', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_employee` (`id_employee`);

--
-- Indices de la tabla `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_profession`);

--
-- Indices de la tabla `professions`
--
ALTER TABLE `professions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `professions`
--
ALTER TABLE `professions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`id_employee`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`id_profession`) REFERENCES `professions` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
