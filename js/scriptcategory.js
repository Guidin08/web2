"use strict";

document.addEventListener("DOMContentLoaded", initPage());

function initPage() {
    let inputCat = document.querySelector("#inputCategory");
    inputCat.addEventListener("focus", function(e) {
        let errorCat = document.querySelector("#errorAddCategory");
        errorCat.innerHTML = "";
    })

    let btnEditCat = document.querySelectorAll(".btn-editCat");
    let menuCategory = document.querySelector("#menuCategories");
    btnEditCat.forEach(b => b.addEventListener("click", function(e) {
        e.preventDefault();
        editarCategoria(b.id);
    }));

    async function editarCategoria(id) {
        let response = await fetch(`editCategory/${id}`, {
            "method": "GET"
        });
        let text = await response.text();
        menuCategory.innerHTML += text;
    }

    let btnDelCat = document.querySelectorAll(".btn-delCat");
    btnDelCat.forEach(b => b.addEventListener("click", function(e) {
        e.preventDefault();
        eliminarCategoria(b.id);
    }));

    async function eliminarCategoria(id) {
        let r = await fetch(`deleteCategory/${id}`, {
            "method": "GET"
        });
        let text = await r.text();
        menuCategory.innerHTML += text;
    }
}