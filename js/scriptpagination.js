"use strict";

document.addEventListener("DOMContentLoaded", initPagination());
const list = new Vue({
    el: "#app",
    data: {
        list: [],
    },

});

function initPagination() {
    let posicion = 0;
    let profesiones = document.querySelectorAll('#listado');
    let search = document.querySelector('#buscar');
    let ok = true;
    let izq = document.querySelector('#izquierda');
    let der = document.querySelector('#derecha');


    search.addEventListener('click', function(e) {
        e.preventDefault();
        posicion = 0;
        let filtroadv = document.getElementsByName('buscar')[0].value;
        izq.disabled = false;
        der.disabled = false;
        searchAdvEmployees(filtroadv, posicion);
        ok = false;
    });
    profesiones.forEach(prof => prof.addEventListener('click', function(e) {
        e.preventDefault();
        ok = false;
        posicion = 0;
        izq.disabled = false;
        der.disabled = false;
        document.getElementsByName('buscar')[0].value = prof.name;
        searchAdvEmployees(prof.name, posicion);
    }));

    /* manejo de botones de paginacion con los if controlo de no pasarme en el arreglo */
    izq.addEventListener('click', function(e) {
        e.preventDefault();
        if (posicion > 0) {
            izq.disabled = false;
            der.disabled = false;
            posicion -= 1;
            if (ok) {
                loademployees(posicion);
            } else {
                let filtroadv = document.getElementsByName('buscar')[0].value;
                searchAdvEmployees(filtroadv, posicion);
            }
        } else {
            izq.disabled = true;
        }
    });
    der.addEventListener('click', function(e) {
        e.preventDefault();
        if (list.list.length >= 3) {
            der.disabled = false;
            izq.disabled = false;
            posicion += 1;
            if (ok) {
                loademployees(posicion);
            } else {
                let filtroadv = document.getElementsByName('buscar')[0].value;
                searchAdvEmployees(filtroadv, posicion);
            }
        } else {
            der.disabled = true;
        }
    });

    let params = new URLSearchParams(location.search);
    let searchNav = params.get('buscar');
    document.getElementsByName('buscar')[0].value = searchNav;
    console.log(document.getElementsByName('buscar')[0].value);
    if (!(searchNav)) {
        loademployees(posicion);
    } else {
        ok = false;
        searchAdvEmployees(searchNav, posicion);
    }

}

/* obtengo posicionidad limitada de empleados para la paginacion */
async function loademployees(posicion) {
    try {
        let load = await fetch(`api/employees/pagina/${posicion}`, {
            "method": "GET",
        });
        if (load.ok) {
            let listempl = await load.json();
            list.list = listempl;
        }
    } catch {
        noFoundEmployees();
    }

}

/* busqueda avanzada */
async function searchAdvEmployees(filt, pos) {
    try {
        let load = await fetch(`api/employees/adv/${filt}/${pos}`, {
            "method": "GET",
        });
        if (load.ok) {
            let empl = await load.json();
            list.list = empl;
        }
    } catch {
        noFoundEmployees();
    }
}

/* en caso de no tener ningun empleado en la categoria creo un aviso */
function noFoundEmployees() {
    /* list.list=[{apellido:'No Hay Empleados',nombre:'Para Esta Categoria',emp_image:'./img/singente.jpg'}]; */
    list.list = [];
}