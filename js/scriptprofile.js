"use strict";
const opinions = new Vue({
    el: "#opinions",
    data: {
        comentarios: [], 
        isAdmin: 0,
        idEmployee: 0,
        idUser: 0,
    },
    methods: {
        deleteComment: async function(e) {
            let id = e.target.id;
            console.log(id);
            const response = await fetch(`api/comments/${id}`, {
                "method": "DELETE",
            });
            if (response.ok) {
                createLoad();
                let employee = document.querySelector('#employe').name
                loadAllComentaries(employee);
            }
        }
    }
});

document.addEventListener("DOMContentLoaded", initProfile());

function initProfile() {
    opinions.isAdmin = document.querySelector("#isAdmin").value;
    opinions.idEmployee = document.querySelector('#employe').name;
    opinions.idUser = document.querySelector("#idUser").value;
    loadAllComentaries(opinions.idEmployee);
    let comment = document.querySelector('#annotation');
    //let stars = document.querySelectorAll('#stars');
    let stars = document.querySelectorAll('input[name=estrellas]');
    let valora;
    stars.forEach(star => star.addEventListener('click', function(e) {
        valora = star.value;
    }));
    let califica = document.querySelector('#calificar');
    if (califica) {
        califica.addEventListener('click', function(e) {
            e.preventDefault();
            calificar(valora, comment.value, califica.name, opinions.idUser);
        });
    }
}

async function calificar(stars, opinions, id_employ, user) {
    if (opinions == '') {
        opinions = 'Sin Comentario de parte del usuario';
    }
    if (stars == undefined) {
        stars = 5;
    }
    let data = {
        "valoracion": stars,
        "comentario": opinions,
        "id_user": user,
        "id_employee": id_employ
    }
    let comment = await fetch(`api/comments`, {
        "method": "POST",
        "headers": { "Content-Type": "application/json" },
        "body": JSON.stringify(data)
    })
    if (comment.ok) {
        createLoad();
        loadAllComentaries(id_employ);
    }
}

async function loadAllComentaries(id) {
    let loadcomment = await fetch(`api/comments/${id}`);
    if (loadcomment.ok) {
        let comments = await loadcomment.json();
        let fincarga = document.querySelector("#loader");
        fincarga.remove();
        opinions.comentarios = comments;
    }
}

/* function addComment(comentario, isAdmin) {
    let divcomment = document.createElement('div')
    let item = document.createElement('li');
    let user = document.createElement('span');
    let score = document.createElement('span');
    let comment = document.createElement('p');
    divcomment.className = 'mt-1';
    item.className = "list-group-item list-group-item-primary p-0 text-left";
    user.className = "ml-2 w-100";
    user.innerHTML = comentario.user;
    score.className = 'float-right mr-1';
    let puntaje = parseInt(comentario.valoracion)
    for (let i = 0; i < 5; i++) {
        if (i < puntaje) {
            score.innerHTML += ' ★';
        } else score.innerHTML += ' ☆'
    }
    comment.className = 'text-sm-center';
    comment.innerHTML = comentario.comentario;
    item.appendChild(user);
    item.appendChild(score);
    item.appendChild(comment);
    divcomment.appendChild(item);
    if (isAdmin == 1) {
        let btnDel = document.createElement('button');
        btnDel.addEventListener('click', e => {
            deleteComment(comentario.id);
        });
        btnDel.className = 'btn btn-danger btn-sm float-right mb-1';
        btnDel.innerHTML = '<i class="fas fa-eraser"></i>';
        btnDel.innerHTML += ' Eliminar';
        divcomment.appendChild(btnDel);
    }
    addopinions.appendChild(divcomment);
} */

function createLoad() {
    let load = document.querySelector("#load");
    load.innerHTML = "<div class=lds-spinne id=loader><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>";
}