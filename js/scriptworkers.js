"use strict";

document.addEventListener("DOMContentLoaded", initPage());

function initPage() {
    let buttonsEditPerson = document.querySelectorAll(".btn-editPers");
    let menuWorkers = document.querySelector("#menuWorkers");
    buttonsEditPerson.forEach(b => b.addEventListener("click", function(e) {
        e.preventDefault();
        editarPersona(this.id);
    }));

    async function editarPersona(id) {
        let r = await fetch(`editPerson/${id}`, {
            "method": "GET"
        });
        let html = await r.text();
        menuWorkers.innerHTML += html;
        let btnDelImage = document.querySelector("#deleteImage");
        btnDelImage.addEventListener("click", e => {
            e.preventDefault();
            document.querySelector("#imagePerson").value = "";
            document.querySelector("#imgPerson").src = "./img/foto.jpg";
        });
    }


    let btnAddPers = document.querySelector("#btn-addPerson");
    btnAddPers.addEventListener("click", function(e) {
        e.preventDefault();
        showAddPerson();
    });

    async function showAddPerson() {
        let r = await fetch(`showAgreePerson`);
        let html = await r.text();
        menuWorkers.innerHTML += html;
    }

    let btnDelPers = document.querySelectorAll(".btn-delPers");
    btnDelPers.forEach(b => b.addEventListener("click", function(e) {
        e.preventDefault();
        showDelPers(b.id);
    }));

    async function showDelPers(id) {
        let r = await fetch(`deleteWorker/${id}`, {
            "method": "GET"
        });
        let html = await r.text();
        menuWorkers.innerHTML += html;
    }


}