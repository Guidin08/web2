<?php
    /* Incluyo la libreria para el ruteo */
    include_once 'lib/Router.php';
    /* Incluyo el controlador de profesiones y comemtarios*/
    include_once 'app/api/api-profession.controller.php';
    include_once 'app/api/api-comments.controller.php';
    include_once 'app/api/api-employees.controller.php';

    /* creo el ruteo */
    $router = new Router();

    /* Creando la tabla de ruteo */
    
    $router->addRoute('profesion', 'GET', 'ApiProfessionController' , 'getAll');
    $router->addRoute('profesion/:ID','GET', 'ApiProfessionController', 'getselected');
    $router->addRoute('profesion/valoracion/:ID','GET', 'ApiProfessionController', 'getselected');
    $router->addRoute('profesion/:ID', 'GET', 'ApiProfessionController' , 'get');
    $router->addRoute('profesion/:ID', 'DELETE', 'ApiProfessionController' , 'delete');
    $router->addRoute('profesion', 'POST', 'ApiProfessionController', 'add'); // no uso parametros porque es POST
    $router->addRoute('profesion/:ID', 'PUT', 'ApiProfessionController', 'update');
    $router->addRoute('comments', 'POST', 'ApiCommentsController', 'add');
    $router->addRoute('comments/:ID', 'GET', 'ApiCommentsController', 'getAll');
    $router->addRoute('comments/:ID', 'DELETE', 'ApiCommentsController', 'delete');
    $router->addRoute('employees', 'GET', 'ApiEmployeesController' , 'getAll');
    $router->addRoute('employees/pagina/:ID', 'GET', 'ApiEmployeesController' , 'getPagination');
    $router->addRoute('employees/:ID', 'GET', 'ApiEmployeesController' , 'get');
    $router->addRoute('employees/adv/:FILTER/:POS', 'GET', 'ApiEmployeesController' , 'getadv');

    /* rutea -> obteniendo el RECURSO y el METODO por el que me llamaron */
    $router->route($_GET['resource'], $_SERVER['REQUEST_METHOD']);
