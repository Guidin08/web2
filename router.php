<?php
 
define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

include_once 'app/controllers/profession.controller.php';
include_once 'app/controllers/auth.controller.php';
//phpinfo();
if (!empty($_GET['action'])) {
    $iniciar = $_GET['action'];
} else {
    $iniciar = 'home'; // inicio acción por defecto si no envían
}

$params = explode('/', $iniciar);
switch ($params[0]) {
    //lama al controlador para pasarle la accion correspondiente
    case 'home':
        $controller = new JobController();
        $controller->showHome();
    break;      
    case 'login':
        $controller = new AuthController();
        $controller-> initSesion();               
    break; 
    case 'verifyLogin':
        $controller = new AuthController();
        $controller-> verinicio();        
    break;  
    case 'manageCategory':
        $controller = new JobController();
        $controller->showMenuManageCat('');
    break;
    case 'manageWorkers':
        $controller = new JobController();
        $controller->showMenuManageWork('');
    break;
    case 'manageUsers':
        $controller = new JobController();
        $controller->showMenuManageusers('');
    break;
    case 'addCategory':
        $controller = new JobController();
        $controller->addCategory();
    break;
    case 'deleteCategory':
        $controller = new JobController();
        $controller->deleteCategory($params[1]);
    break;
    case 'deleteWorker':
        $controller = new JobController();
        $controller->deleteWorker($params[1]);
    break;
    case 'editCategory':
        $controller = new JobController();
        $controller->showEditCategory($params[1]);
    break;
    case 'updateCategory':
        $controller = new JobController();
        $controller->updateCategory();
    break;
    case 'editPerson':
        $controller = new JobController();
        $controller->showEditPerson($params[1]);
    break;
    case 'updatePerson':
        $controller = new JobController();
        $controller->updatePerson();
    break;
    case 'showAgreePerson':
        $controller = new JobController();
        $controller->showAgreePerson();
    break;
    case 'addPerson':
        $controller = new JobController();
        $controller->addPerson();
    break;
    case 'seeProfile':
        $controller =new JobController();
        $controller-> verPerfil($params[1]);
    break;    
    case 'optionAcount':
        $controller= new JobController();
        $controller-> showOptions();    
    break; 
    case 'createAcount':
        $controller= new JobController();
        $controller-> createAcount();    
    break; 
    case 'newAcountUser':
        $controller= new JobController();
        $controller-> createAcountUser();    
    break;
    case 'upPerson':
        $controller= new JobController();
        $controller->upPerson();   
    break;
    case 'logout':
        $controller= new AuthController();
        $controller->logout();    
    break;
    case 'adduser':
        $controller = new AuthController();
        $controller->createUser();
    break;
    case 'changeUser':
        $controller = new AuthController();
        $controller->changeUser($params[1]);
    break;
    case 'deleteUser':
        $controller = new AuthController();
        $controller->deleteUser($params[1]);
    break;
    default:
        header("HTTP/1.0 404 Not Found");
        echo('404 Page not found');
    break;
} 