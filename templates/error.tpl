
  <div class='container'>
    <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">ERROR !</h4>
        <p>{$msg}</p>
        <hr>
        <p class="mb-0"></p>
    </div>
  </div>
{include 'footer.tpl'}
