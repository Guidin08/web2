{include 'head.tpl'}
     
        <header class="sticky-top">
            <nav class="navbar navbar-expand-lg navbar-light bg-info">
                <a class="navbar-brand" href="home" action="home" method="GET">JobServices <i class="fas fa-home"></i></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>        
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav row w-100 m-0 justify-content-around">
                        <div class="col-sm-12 col-md-7 m-0">
                            <form class="form-inline my-2 my-lg-0 justify-content-center" action="home" method="GET">
                                <input class="form-control mr-sm-2 w-75" type="search" name="buscar" placeholder="Buscar" aria-label="Search">
                                <button class="btn btn-success my-2 my-sm-0" type="submit" id='buscar'>Buscar
                                    <i class="fas fa-search"></i>
                                </button>
                            </form>
                        </div>
                        <div class="col-sm-12 col-md-4 d-flex justify-content-between m-0">
                        {if isset($smarty.session.USUARIO)}
                            {if $smarty.session.ADMIN == 1}
                            <li class="nav-item">                                
                                    <a class="nav-link" href="manageCategory">Administrar</a>                                    
                            </li>
                            {/if}
                            <li class="nav-item d-flex w-100 justify-content-end">
                                    <p class="my-auto">Hola, {$smarty.session.USUARIO}!</p>
                                    <a class="nav-link ml-2" href="logout">Cerrar Sesion   
                                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-power" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M5.578 4.437a5 5 0 1 0 4.922.044l.5-.866a6 6 0 1 1-5.908-.053l.486.875z"/>
                                            <path fill-rule="evenodd" d="M7.5 8V1h1v7h-1z"/>
                                        </svg>
                                    </a>       
                            </li>                      
                        {else}                       
                            <li class="nav-item d-flex w-100 justify-content-end">
                                <a class="nav-link" href="optionAcount">Registrarme</a>
                                <a class="nav-link ml-2" href="login">Iniciar Sesion</a>
                            </li>
                        {/if} 
                        </div>
                    </ul>
                    
                </div>             
            </nav>
        </header>
        <body>
            <main class='row m-0 overflow-auto'>
          



