<section class='row' > 
  <button class='ml-5 align-self-center justify-content-center buttonredondo buttonflecha' id='izquierda' >
   <img src="img/imghome/izq.png" height="50" width="50">  
  </button>

  <div id="resul" class='col w-70'>
    {include file='vue/list.vue'}
  </div>  
  
  <button class='ml-5 align-self-center justify-content-center buttonredondo buttonflecha' id='derecha'>
    <img src="img/imghome/der.png" height="50" width="50">
  </button>
</section>

{include 'footer.tpl'}  

<script type="text/javascript" src='js/scriptpagination.js'></script>