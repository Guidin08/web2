<div class='row w-75'>
        {foreach from=$employees item=$employee }
          <div class='card row ml-5 mt-5' style='width: 18rem'>
              {if ($employee->emp_image==null)}
                <img src='./img/foto.jpg' width="350" height="250" class="card-img" alt="...">
              {else}
                <img src="{$employee->emp_image}" width="350" height="250" class="card-img" alt="...">
              {/if}
              <div class='card-body'>
                <p class='card-text'>{$employee->apellido}</p>
                <p class='card-text'>{$employee->nombre}</p>   
                <a class='btn btn-info' href='seeProfile/{$employee->id_emp}'>Ver datos de Contacto</a> 
              </div>
          </div>
         
                   
        {/foreach}
      </div>
     <p id="agregar"></p>