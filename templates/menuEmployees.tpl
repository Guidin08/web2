{include 'header.tpl'}

<div id="menuWorkers" class="container mt-5">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item" role="presentation">
        <a class="nav-link" href="manageCategory" aria-selected="false">Profesiones</a>
      </li>
      <li class="nav-item" role="presentation">
          <a class="nav-link active" href="manageWorkers" aria-selected="true">Personas</a>
      </li>
      <li class="nav-item" role="presentation">
          <a class="nav-link" href="manageUsers" aria-selected="false">Usuarios</a>
      </li>
    </ul>
    
    <div class="d-flex justify-content-between">
      <p class="text-center text-danger m-0">{$msg}</p>
      <a id="btn-addPerson" type="button" class="btn btn-primary" href="showAgreePerson">Agregar persona</a>
    </div>

    <div class="tab-content overflow-auto" id="myTabContent">
        <table class="table">
          <caption>Lista de trabajadores</caption>
          <thead>
            <tr>
              <th scope="col">DNI</th>
              <th scope="col">Nombre</th>
              <th scope="col">Email</th>
              <th scope="col">Telefono</th>
              <th scope="col">Profesion</th>
              <th scope="col">Funciones</th>
            </tr>
          </thead>
          <tbody>
            {foreach from=$employees item=worker}              
                <tr>
                  <td>{$worker->dni}</td>
                  <td>{$worker->nombre} {$worker->apellido}</td>
                  <td>{$worker->email}</td>
                  <td>{$worker->telefono}</td>
                  <td>{$worker->profesion}</td>
                  <td class="d-flex no-wrap">
                    <a class="btn btn-outline-warning btn-sm btn-editPers" id={$worker->id_emp}>Editar</a>
                    <a class="btn btn-outline-danger btn-sm btn-delPers" href="deleteWorker/{$worker->id_emp}" id="{$worker->id_emp}">Eliminar</a>
                  </td>
                </tr>    
            {/foreach} 
            {if $employees|@count == 0}
              <tr><td> No se han encontrado trabajadores </td></tr>
            {/if}
          </tbody>
        </table>
    </div> 
  </div>


  <script type="text/javascript" src='js/scriptworkers.js'></script>
{include 'footer.tpl'}