{include "header.tpl"}

    <div id="menuCategories" class="container mt-5">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" href="manageCategory" aria-selected="true">Profesiones</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="manageWorkers" aria-selected="false">Personas</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="manageUsers" aria-selected="false">Usuarios</a>
            </li>
        </ul>
        
        <form id="formAddCategory" class="form-inline mt-2 d-flex justify-content-around" action="addCategory" method="POST">
            <div class="form-group mx-sm-3 mb-2">
                <label for="inputCategory" class="sr-only">Profesion</label>
                <input type="text" class="form-control" name="inputCategory" id="inputCategory" placeholder="Nombre de profesion" required>
            </div>
            <button type="submit" class="btn btn-primary mb-2">Agregar</button>
        </form>
        {if $msg != null}
            <p id="errorAddCategory" class="text-center text-danger m-0">{$msg}</p>
        {/if}
        <div class="tab-content" id="myTabContent">
            <table class="table">
            <caption>Lista de profesiones</caption>
            <thead>
                <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Funciones</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$professions item=$profession}
                    <tr>
                        <td>{$profession->profesion}</td>
                        <td class="d-flex no-wrap">
                            <a class="btn btn-outline-warning btn-sm btn-editCat" id={$profession->id}>Editar</a>
                            <a class="btn btn-outline-danger btn-sm btn-delCat" id={$profession->id}>Eliminar</a>
                        </td>
                    </tr> 
                {/foreach}
            </tbody>
            </table>
        </div> 
    </div>

    <script type="text/javascript" src="js/scriptcategory.js"></script>
{include "footer.tpl"}