{include "header.tpl"}

    <div id="menuCategories" class="container mt-5">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="manageCategory" aria-selected="false">Profesiones</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="manageWorkers" aria-selected="false">Personas</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link active" href="manageUsers" aria-selected="true">Usuarios</a>
            </li>
        </ul>    

        {if $msg != null}
            <p id="errorAddCategory" class="text-center text-danger m-0">{$msg}</p>
        {/if}
        <div class="tab-content" id="myTabContent">
            <table class="table text-center">
            <caption>Lista de Usuarios</caption>
            <thead>
                <tr>
                <th scope="col">Nombre</th>
                <th scope="col">¿es administrador?</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$users item=$user}
                    <tr>
                        <td>{$user->user}</td>
                        <td>
                            {if $user->type_user == 0}
                               <span class="badge badge-success">NO</span>
                            {else}
                                <span class="badge badge-danger">SI</span>
                            {/if}
                            </span>
                        </td>
                        <td class="d-flex no-wrap justify-content-end">
                            {if $user->type_user == 0}
                                <a class="btn btn-outline-warning btn-sm" id={$user->id} href='changeUser/{$user->id}'>Administrador</a>
                            {/if}
                            <a class="btn btn-outline-danger btn-sm ml-2" id={$user->id} href='deleteUser/{$user->id}'>Eliminar</a>
                        </td>
                    </tr> 
                {/foreach}
            </tbody>
            </table>
        </div> 
    </div>

{include "footer.tpl"}