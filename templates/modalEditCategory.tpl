<aside class="modal-result d-flex justify-content-center align-items-center">
        <div class="confirmacion-texto bg-white p-5 rounded-left rounded-right">
            <h3>Edición de categoria</h3>
            <form action="updateCategory" method="POST">
                <input type="text" class="form-control mb-2" name="nameCategory" id="nameCategory" value="{$category->profesion}" required>
                <input type="hidden" name="idCategory" value="{$category->id}">
                <button type="sumbit" class="btn btn-outline-warning btn-sm">Modificar</button>
                <a type="button" class="btn btn-outline-info btn-sm" href="manageCategory">Cancelar</a>
            </form>
        </div> 
</aside>
