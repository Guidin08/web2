
{if $person->id_emp == ""}
    {$link = "addPerson"}
{else} {$link = "updatePerson"}
{/if}

<aside id="confirmregister" class="modal-result d-flex justify-content-center align-items-center">
    <div class="confirmacion-texto bg-white p-5 rounded-left rounded-right">
        <form action={$link} method="POST" enctype="multipart/form-data">
            <div class="form-row">
                <div class="form-group col-md-4">
                    {if ($person->emp_image=="")}
                        <img id="imgPerson" class='rounded-circle' src='./img/foto.jpg' width="150" height="150" alt="...">
                    {else}
                        <img id="imgPerson" class='rounded-circle' src="{$person->emp_image}" width="150" height="150" alt="...">
                    {/if}
                    <button id="deleteImage" class="btn btn-outline-danger btn-sm float-right">-</button>
                    <input type="hidden" name="id" value={$person->id_emp}>
                </div>
                <div class="form-group col-md-8">
                    <div class="row">
                        <label for="dni" class="col-sm-2 col-form-label">DNI</label>
                        <div class="col-sm-10"> 
                            <input type="number" class="form-control" name="dni" placeholder="DNI" value={$person->dni}
                            {if !($person->id_prof == "")}
                                {"readonly"}
                            {/if}>
                        </div>
                    </div>
                    <label for="nombre">Nombre y Apellido</label>
                    <input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{$person->nombre}">
                    <input type="text" class="form-control mt-2" name="apellido" placeholder="Apellido" value="{$person->apellido}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-8">
                    <label for="email">E-mail</label>
                    <input type="text" class="form-control" name="email" placeholder="Correo" value={$person->email}>
                </div>
                <div class="form-group col-md-4">
                    <label for="nombre">Telefono</label>
                    <input type="number" class="form-control" name="telefono" placeholder="Telefono" value="{$person->telefono}">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <select class="form-control" name="profesion">
                        {foreach from=$professions item=profesion}
                            <option value="{$profesion->id}"
                                {if {$profesion->id} == {$person->id_prof}} 
                                    {" selected"}
                                {/if}   
                                >{$profesion->profesion}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="form-group col-md-8">
                    <label for="photo">Cambiar la foto</label>
                    <input type="file" name="input_name" id="imageToUpload">
                </div>
            </div>
            <input id="imagePerson" type="hidden" name="foto" value='{$person->emp_image}'>    
            <div class="form-row d-flex justify-content-around">
                <button type="submit" class="btn btn-outline-warning">Aceptar</button>
                <a type="button" class="btn btn-outline-info" href="manageWorkers">Cancelar</a>
            </div>
        </form>
    </div> 
</aside>