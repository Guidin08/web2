<aside id="confirmregister" class="modal-result d-flex justify-content-center align-items-center">
        <div class="confirmacion-texto bg-white p-5 rounded-left rounded-right">
            <h3>{$msg}</h5>
            <a type="button" class="btn btn-outline-info btn mt-2" href={$link}>Cerrar</a>
        </div> 
</aside>