{include 'header.tpl'}
<div class='container mt-5 col-md-4'>
        <form method='POST' action='upPerson' enctype="multipart/form-data" >
            <div class="form-group">
                <label for="apellido">Apellido</label>
                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="" required>
            </div>
            <div class="form-group">
                <label for="nombres">Nombres</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" required>
            </div>
            <div class="form-group">
                <label for="dni">DNI</label>
                <input type="text" class="form-control" id="dni" name="dni" placeholder="" required>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" required>
                </div>                
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>
            </div>    
            <div class="form-group">
                <label for="telefono">Telefono</label>
                <input type="text" class="form-control" id="telefono" name="telefono" placeholder="" required>
            </div>
            
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="ciudad">Ciudad</label>
                    <input type="text" class="form-control" id="ciudad" name="ciudad" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="oficio">Oficio</label>
                        <select id="oficio" class="form-control" name="oficio" >
                            <option hidden >Elegir...</option>
                            {foreach from=$categories item=$category}           
                                <option name={$category->id}>{$category->profesion}</option>
                            {/foreach}                         
                        </select>
                </div>                
            </div> 
            <div class="form-group ">
                <label for="photo">Suba su foto (opcional)</label>
                <input type="file" name="input_name" id="imageToUpload">
            </div>

            <button type="submit" class="btn btn-primary">Registrarme</button>
            <a class='btn btn-info' href='home'>Cancelar</a>

        </form>
</div> 

{include 'footer.tpl'}