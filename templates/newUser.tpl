{include 'header.tpl'}
  <div class='modal-inicio d-flex justify-content-center align-items-center' id='staticBackdrop' data-backdrop='static' tabindex='-1' role='dialog' aria-labelledby='staticBackdropLabel' aria-hidden='true'>
    <div class='confirmacion-texto bg-white p-5 rounded-left rounded-right' role='document'>
      <div class='modal-content'>
        <div class='modal-header'>
          <h5 class='modal-title' id='staticBackdropLabel'>Registrarme</h5>
        </div>
        <div class='modal-body'>
          <form method='POST' action='adduser'>
            <div class='form-group'>
                <label for='usuario'>Usuario</label>
                <input type='usuario' class='form-control' id='usuario' name='usuario' aria-describedby='emailHelp' required>
            </div>
            <div class='form-group'>   
                <label for='password'>Contraseña</label>
                <input type='password' class='form-control' id='password' name='password' required>
            </div>

            <button type="submit" class="btn btn-primary">Registrarme</button>
            <a class='btn btn-primary' href='home'>Cancelar</a>
          </form>

          </div>
          {if ($msg!=null)}
            <div class='alert alert-danger' >{$msg}</div>
          {/if}
      </div>
    </div>
  </div>
{include 'footer.tpl'}