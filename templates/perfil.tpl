
 
{include 'header.tpl'}
  <div class='container mt-2'>
    <div class='row bg-light mb-2'>
      <div class='col text-center'>
        {if ($person->emp_image==null)}
            <img class='rounded-circle' src='./img/foto.jpg' width="200" height="200" alt="...">
        {else}
            <img class='rounded-circle' src="{$person->emp_image}" width="200" height="200" alt="...">
        {/if}
      </div>
      <div class="col-md-8">
        <h3>{$person->apellido} {$person->nombre}</h3>
        <ul class="list-group list-group-flush">
          <li class="list-group-item list-group-item-warning">{$person->profesion}</li>
          <li class="list-group-item">Tel: {$person->telefono}</li>
          <li class="list-group-item">Email: {$person->email}</li>
          <input type='hidden' id="employe" name='{$person->id_emp}'>
        </ul>
      </div>
    </div>
    {if ($ok)}           
  
      <div class="form-check form-check-inline" >
        Elija un puntaje:
        <p class="clasificacion mb-0 ml-2">
          <input class="d-none" id="radio1" type="radio" name="estrellas" value="5"><label class="star mb-0" for="radio1"> ★</label>
          <input class="d-none" id="radio3" type="radio" name="estrellas" value="4"><label class="star mb-0" for="radio3"> ★</label>
          <input class="d-none" id="radio2" type="radio" name="estrellas" value="3"><label class="star mb-0" for="radio2"> ★</label>
          <input class="d-none" id="radio4" type="radio" name="estrellas" value="2"><label class="star mb-0" for="radio4"> ★</label>
          <input class="d-none" id="radio5" type="radio" name="estrellas" value="1"><label class="star mb-0" for="radio5"> ★</label>
        </p>
      </div>

      <textarea class="form-control" rows="4" cols="3" name="annotation" id="annotation" maxlength="250" placeholder="Escriba aqui su comentario acerca de esta persona..." ></textarea>                        
      <div class="text-right">
        <button type='submit' class="btn btn-success" id="calificar" name='{$person->id_emp}'>Calificar</button>
      </div>
    {/if}
    <div id="load">
      <div class="lds-spinner" id="loader">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
      </div>
    </div>
    {if isset($smarty.session.ADMIN)}
      <input type="hidden" id="isAdmin" value={$smarty.session.ADMIN}>
    {else}
      <input type="hidden" id="isAdmin" value=0>
    {/if}
    {if isset($smarty.session.ADMIN)}
      <input type="hidden" id="idUser" value={$smarty.session.ID_USUARIO}>
    {else}
      <input type="hidden" id="idUser" value=0>
    {/if}
    <ul class='list-group' id="opinions">                            
        {include file='vue/listComments.vue'}
    </ul>
  </div>
  <script type="text/javascript" src='js/scriptprofile.js'></script>

{include 'footer.tpl'}